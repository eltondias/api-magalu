const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const cors = require('cors')({ origin: true });
const admin = require('firebase-admin');


module.exports = { 
    sendMail: (email, assunto, bodyMail, callBack) => {

        const transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'desafio.api.magalu@gmail.com',
                pass: 'd3s4f10maga'
            }
        });

        if (ValidateEmail(email.toLowerCase))
            return res.status(400).send({ status: "E-mail inválido." });

        let remetente = '"MAGALU" <contato@megalu.com>';         
        let destinatarios = email;
        let corpoHtml = bodyMail;
        let corpo = corpoHtml;
        let mensagem = {
            from: remetente,
            to: destinatarios,
            bcc: "elton.jd.goncalves@gmai",
            subject: assunto,
            // text: corpo,
            html: corpoHtml
        };

        transporter.sendMail(mensagem, (error, info) => {
            if (error) {
                console.log(error)
                return   callBack(error);
            }
            return   callBack({
                success: 1,
                mensagem: "send email successfully"
            });
        });


    }
}


function ValidateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}



