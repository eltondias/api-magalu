const pool = require("../../config/database");
const { sendMail } = require("../services/sendmail.service");
const json2html = require('node-json2html');
const pdf = require("pdf-creator-node");
const fs = require("fs");

const getPedido = (id, callBack) => {
  return new Promise((resolve, reject) => {
    pool.query(`select * from pedido where id = ?`, [id], (error, elements) => {
      if (error) {
        return callBack(error);
      }
      return resolve(elements[0]);
    });
  });
};

const getCliente = (id, callBack) => {
  return new Promise((resolve, reject) => {
    pool.query(`select c.* from pedido p inner join cliente c on c.id = p.cliente_id where p.id = ?`, [id], (error, elements) => {
      if (error) {
        return callBack(error);
      }
      return resolve(elements[0]);
    });
  });
};

const getProdutosPedido = (id, callBack) => {
  return new Promise((resolve, reject) => {
    pool.query(`
    select  pp.*, pr.*
    from produto_pedido pp 
    inner join pedido p on p.id = pp.pedido_id
    inner join produto pr on pr.id = pp.produto_id
    where pp.pedido_id = ?`, [id], (error, elements) => {
      if (error) {
        return callBack(error);
      }
      return resolve(elements);
    });
  });
};


const resumePedido = async (pedido_id, callBack) => {
  const cliente = await getCliente(pedido_id, callBack);
  const pedido = await getPedido(pedido_id, callBack);
  const produtos = await getProdutosPedido(pedido_id, callBack);
  pedido.produtos = produtos;

  const dados = {
    "cliente": cliente,
    "pedido": pedido
  }
  return dados;
}

const toTable = (dados) => {

  let htmlPedido =
    ` 
  <tr> <th colspan="8"> <h2> DADOS PEDIDO </h2></th></tr>
  <tr>
      <th>Código</th>                            
      <th>Forma pag.</th>                  
      <th>Data</th>                    
      <th colspan="4" >Observação</th>                     
  </tr>`;

  htmlPedido += `
      <tr>
        <td>${dados.pedido.id}</td>  
        <td>${dados.pedido.forma_pagamento}</td>                    
        <td>${dados.pedido.data}</td>  
        <td colspan="4">${dados.pedido.observacao}</td>  
      </tr>`;


  let htmlCliente =
    `
  <tr> <th colspan="8"> <h3> CLIENTE: </h3></th></tr>
   <tr>
          <th>Código</th>        
          <th colspan="4" >Nome</th>                            
          <th>Cpf</th>                  
          <th >Sexo</th>                                                
      </tr>`;

  htmlCliente += `
      <tr>
        <td>${dados.cliente.id}</td>  
        <td  colspan="4">${dados.cliente.nome}</td>                    
        <td>${dados.cliente.cpf}</td>  
        <td>${dados.cliente.sexo}</td>  
      </tr>`;

  let htmlProdutos =
    ` 
  <tr> <th colspan="8"> <h3> PRODUTOS: </h3></th></tr>

  <tr>
        <th>Código</th>         
        <th>Produto</th>                            
        <th>Cor</th>                  
        <th>Tamanho</th>         
        <th>Qtd.</th>       
        <th>Valor</th>       
        <th>Subtotal</th>           
  </tr>`;

  dados.pedido.produtos.forEach(produto => {
    let htmlProduto = `<tr>`;
    htmlProduto += `<td>${produto.id}</td>`
    htmlProduto += `<td>${produto.nome}</td>`
    htmlProduto += `<td>${produto.cor}</td>`
    htmlProduto += `<td>${produto.tamanho}</td>`
    htmlProduto += `<td>${produto.quantidade}</td>`
    htmlProduto += `<td>${produto.valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</td>`
    htmlProduto += `<td>${(produto.valor * produto.quantidade).toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })}</td>`
    htmlProduto += `</tr>`;
    htmlProdutos += htmlProduto;
  });
  htmlProdutos +=
    ` <tr>
          <th colspan="6">  <h3> Total  </h3></th>                                   
          <th> <h3> ${dados.pedido.total.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })} </h3> </th>           
    </tr>`;

  let bodyMail =
    `<table border="1" width="90%" cellpadding="2" cellspacing="0" style="1px solid rgb(0 0 0 / 14%)"> 
 
  ${htmlPedido}
  ${htmlCliente}
  ${htmlProdutos}
</table>`;

  return bodyMail;
}


module.exports = {

  create: (data, callBack) => {
    const createProdutoPedido = (produto, pedido_id) => {
      pool.query(
        `insert into produto_pedido(quantidade, valor, pedido_id, produto_id) values(?,?,?,?)`,
        [
          produto.quantidade,
          produto.valor * produto.quantidade,
          pedido_id,
          produto.id,
        ],
        (error, response) => {
          if (error) {
            console.log(error)
          }
          console.log(response)
        }
      );
    }

    let total = 0;
    data.produtos.forEach(produto => {
      total += total + (produto.valor * produto.quantidade);
    });

    pool.query(
      `insert into pedido(observacao, forma_pagamento, total, data, cliente_id) values(?,?,?,?,?)`,
      [
        data.observacao,
        data.forma_pagamento,
        total,
        data.data,
        data.cliente_id,
      ],
      (error, response) => {

        if (error) {
          callBack(error);
        }

        data.total = total;
        data.id = response.insertId;

        data.produtos.forEach(produto => {
          createProdutoPedido(produto, response.insertId);
        });

        return callBack(null, data);

      }
    );
  },

  getList: callBack => {
    pool.query(
      `select * from pedido`,
      [],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  },

  getById: (id, callBack) => {
    pool.query(
      `select * from pedido where id = ?`,
      [id],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response[0]);
      }
    );
  },

  update: (data, callBack) => {
    pool.query(
      `update pedido set observacao=?, forma_pagamento=?, total=?, data=?, cliente_id=? where id = ?`,
      [
        data.observacao,
        data.forma_pagamento,
        data.total,
        data.data,
        data.cliente_id,
        data.id,
      ],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  },

  deleteItem: (id, callBack) => {
    pool.query(
      `delete from pedido where id = ?`,
      [id],
      (error, response) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  },


  sendMailById: async (email, pedido_id, callBack) => {
    const dados = await resumePedido(pedido_id, callBack)
    let bodyMail = `

    <p>
      Olá ${(dados.cliente.nome.split(" "))[0]}, segue os detalhes do seu pedido de compra.
    </p>
    
    `;
    bodyMail += toTable(dados);
    bodyMail += `

      <p>
       Att, 
       <br> 
       Magazine Luiza.
    
      </p>
      <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJcAlwMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUCAwYBB//EADsQAAEDAgQEBAMFBQkAAAAAAAABAgMEEQUGElETITGRBxRBgTJhcRUiQ7HwUnOhweEWIyQ0U2KCorL/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAwQFAQYC/8QAOBEAAgEDAgEICAUEAwAAAAAAAAECAwQRBRIxEyEiQVFhkdEGFDJxgaGxwRZEUlTwNEJDchUjM//aAAwDAQACEQMRAD8AiXduvc99hHh8sXduvcYQyxd269xhDLF3br3GEMsXduvcYQyxd269xhDLF3br3GEMsXduvcYQyxd269xhDLF3br3GEMsXduvcYQyxd269xhDLF3br3GEMsXduvcYQyxd269xhDLF3br3GEMsXduvcYQyxd269xhDLF3br3GEMs8OnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALgAAXAAAum4AugAAAAAAAAAAAABJwyidiGI09G1yMWd6M1L6XIa9XkacqnYTUKXK1FDtLTM2Wp8B4D1m48MqqmtG20uT0/WxVsdQjdZWMNFi8spW2HnKZnljK0+PRSz8ZIIGLpa5W6tbvXt/M+b7UY2slDGWfVnYSuIuWcIi4bgFXiWLT0FOqf3EjmSzO+FqIqpf3t0Ja19To0Y1JdfBEVKzqVarpx6us6Oo8O3JT6qbEEdOiX0vZZq+/oZkdb6XThzGjLR+boy5ymzFlabBKWlldUJO+Z+hWNZzR1uic+exetNRVxOSxhIp3Wnu3jF5y28Fth3h7JLTJJiFZwJHJfhxsvp+SqU62tRUsU45XeWqWkNxzUlh9hizw8qnSytkr40jS3Dc1l1dy53S/Kx163HasQC0ebk8z5uopctZaqsfV0kb2xU0a6XSuS93dbIm/TuXb3UKdrhNZbKVpYzuufgi+rPD6RunyVe2TmiPbIyyol+apb8ihT1tc++Beno/6JeJAkyZKzH4MLStaqSwLMsmj4bLa1r/AEJ46rm3dbbweCCWmtV1S3cVkqMwYS7BcUfROmSazGvR6Nt1L1nc+s0uUxgqXVu7epybeSfljK7swQVEqVSQcF6Mto1Xul9yvfah6rNR25yT2dg7mLluwUuG0619bTUqO0LUSNZqtfTdeperVOSpyn2Ip0afKVIw7S0zPl9cvzQRrUpPxmq6+i1rL/UqWN760pPGMFm8s/VmufOSkL5SAAAABdZNZxM0Ye3Z6u7NVSjqUsWsy5p6zcwPomJtpcbdX4FN9yVkbZGO62v0cn0X9czzNBzttlyudN/xHoq2yvuoPjgywyakoK+DL1GiXhpVleqei6mol/mupVFaFSrB3M+t4+vkKMqdKat4dSyaMtRNjqMd0NbxlrnX+aaWq32uq/xPu9k5RpZ4bfu8nxaRUXVS47vsj5qlViH20kiST+f8x0uurVfoemdOh6vjC24POKpW5fi92T6XmZI/P4Ek1tHnfa+lbfxseZs2+Trbf0/dHo7vHKUs9v2Zz3ic6sSppNKypR8NebV+7rvzv7WNDRVTxLON327ijq7mnHjj7lv4deaXAnea18Piu4Ou/Nvr19LlPVuT5fodnPgtaXv5DpfA1YMssHh4r8PReOlO9yK3rqut1S3r1Pu5UZajipwyj4oNrT80uOGcjkqeq/tLSpBJI7Wq8VLqqKyy3v729zX1OFNWssr3e8y9PqVHcxw/ed3VKiZ8obqnOhkRL+q6jDh/QT/2Rsz/AK2HuZx3iKjkzK92lbLCyy25L1NrR5L1bGetmTqyfrGe4vfC1f8AA1/75v8A5M/XP/aPu+5e0Z/9UvecdlqCVuZMOgWN3EjqW6m25ppXn2sps3lSLtZyzzNGRZxauox60zpfFL/N4f8Au3/mhm6F7M/gaGs+1D4nDm8YoAAAALPLeIxYTjMFdPG+RkaOTSy2rm1U9fqU763lcUXTjxZZtK8aFZVJLJvxzHpK3HZMSw901KqsRjVR1n2t62I7ayVO3VKok+fPcSV7yVSu6tPmMstY/wDZeNyYhXpNUcWJzHuRdT1VVRb81/22OXtly1BUqeFh57uvzO2l5yNZ1J5eV5eRKps2Oo8xVmI00T1pqpbvgeqIq2Tkt/RepFPTOUto05PpR6yWOouFxKol0X1FxU54wpquqaTC5HVqpbXKxjf+yKqlKGj3Hszn0e7PkW5arQ9qEOl348ypzRmmLGaOjZBDNDPA/iPctrarenPcuWOmyt5ycmmnzFS8v1XhFRWGucu6XNWKR0MLMQwCpqHuYipJGxbPRU5KqWVChPTqDm3SqpL+d5fhfVlBcpSbZf4RX1cuHzV+JweTjS7mRL1YxE6r9TPuKVONRU6T3d/f3F2hVnKm51Ft7uw+f5SzS/A43U08Tp6R7tWlqpqYvqqX6/Q9Df6crl7ovEvqYNjfu3jtksr6FzJnjDad2vC8Kc2SRyLK57WM5X5/Cq3UpLSLiSxVqe7i/qXHqtGLzTgVeNZrSpx+jxTDoZGLTR6bTIn3rqt05KvopbttNcbeVGq+PYVa+ob68atNcO0vIs/4bIxq1WHVGtP2UY9O6qhRei14vozWPii5/wAvSftQfyKiDODaXMNTXU1O/wAjUoxHwLZHJZETUluV+v1LUtKlO3UJPpLg/sVlqShXdSK6L4osavPGGsc6ow7DH+cfyWSVrW8vqiqqkENHrvo1J9HuyWJ6rSXPCHP8Cizfj8GPT0z6eGWNIWKi8W11VV+Smhp1nO1UlN5yUL+7jcuLisYOfNIoAAAAAJdVsiKq/I45JHcZPbOTq13Ybo9owzz2XsN0e07tfYLjKObZdjPLjKO7X2fIXDwwoy7DsqLxDrYYtFVRwTqiIiPa9Y1905/yMWpodOTzCWPhk16erVYxxKGfkVmPZvr8YidT2jpqV3WNjrq75OX1T2Qs2ul0bd7+LK1zf1q624wjn0cn7SdzSwUMMyuBhgAc/RAcHP1RexzIHPZew3IAJpgHQAAAACzwPFGYZJO59OsvEaiIqLZW2MDX9Iq6nThCnU2bXn5dxp6bfws5uUo5yiXUZgjlvalc3/kh5r8F3f7heD8z0NP0poR/wv5FZUVrZejFT3JY+h90vzH18y5D0yto/l34ryIqvVdy3S9F7iHGsn4+ZP8Aji1/bvxXkYKrl9VNKlotSHGf1O/jm0/bvxXkYOa9U+Jf17mjTsZR/uO/jm0f5d+K8jS+mkd+JbuW4UnEgn6ZWsvy78V5Gl1BK78f8y1CSj1FWfpXQl/hfy8jFuGytddZ7+ykrrxfUU6vpFRnwpP5EqKmc3q+5E5p9Rm1dUp1OEPoSo10deZHJZM2rWU+CJMdW1qfBfsQSpN9Zn1KEpPmZt+0GJ+EvdChV06dT+4j9Wl2nqYiz/RXuhmVtArT4VceJ9K2l2kesqG1DmK1mnSlvqaGkaZUsFNTnu3PwJqdNwXORzZJAAAAAAdyAcAAAAOgHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLQ7Y5uR3DGh2w3IYY0O2G5DDGh2w3IYY0O2G5DDGh2w3IYY0O2G5DDGh2w3IYY0O2G5DDGh2w3IYY0O2G5DDGh2w3IYY0O2G5DDGh2w3IYY0O2G5DDGh2w3IYY0O2G5DDGh2w3IYY0O2G5DDP/Z">
      
      
      `;

    sendMail(email, `Detalhes do seu pedido - ${dados.pedido.id}`, bodyMail, callBack);
  },

  reportById: async (pedido_id, callBack) => {
    const dados = await resumePedido(pedido_id, callBack)
    let bodyMail = toTable(dados);

    let pageHtml = ` 
    <!DOCTYPE html>
      <html>
        <head>
          <mate charest="utf-8" />
          <title>Hello world!</title>
        </head>
        <body>
          ${bodyMail}
        </body>
      </html>`;

    var html = await fs.readFileSync(pageHtml, "utf8");

    var options = {
      format: "A3",
      orientation: "portrait",
      border: "10mm",
      header: {
        height: "45mm",
        contents: '<div style="text-align: center;">Author: Shyam Hajare</div>'
      },
      footer: {
        height: "28mm",
        contents: {
          first: 'Cover page',
          2: 'Second page', // Any page number is working. 1-based index
          default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
          last: 'Last Page'
        }
      }
    };

    var document = {
      html: html,
      data: {
        dados: dados,
      },
      path: "./output.pdf",
      type: "",
    };

    pdf
      .create(document, options)
      .then((res) => {
        console.log(res);
        callBack(res)
      })
      .catch((error) => {
        console.error(error);
        callBack(error)
      });


  },


};

