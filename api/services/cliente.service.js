const pool = require("../../config/database");

module.exports = {

  create: (data, callBack) => {
    pool.query(
      `insert into cliente(nome, cpf, sexo, email) values(?,?,?,?)`,
      [
        data.nome,
        data.cpf,
        data.sexo,
        data.email,
      ],
      (error, response) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  },

  getList: callBack => {
    pool.query(
      `select * from cliente`,
      [],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  },

  getById: (id, callBack) => {
    pool.query(
      `select * from cliente where id = ?`,
      [id],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response[0]);
      }
    );
  },

  update: (data, callBack) => {
    pool.query(
      `update cliente set nome=?, cpf=?, sexo=?, email=? where id = ?`,
      [
        data.nome,
        data.cpf,
        data.sexo,
        data.email,
        data.id,
      ],
      (error, response, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response[0]);
      }
    );
  },

  deleteItem: (id, callBack) => {
    pool.query(
      `delete from cliente where id = ?`,
      [id],
      (error, response) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, response);
      }
    );
  }


};
