
const { create, getList, getById, update, deleteItem, sendMailById, reportById } = require("../services/pedido.service");

module.exports = {

  create: (req, res) => {
    const body = req.body;
    create(body, (err, response) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: err.sqlMessage
        });
      }  
      return res.status(200).json(response);
    });
  },

  getList: (req, res) => {
    getList((err, response) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json(response);
    });
  },

  getById: (req, res) => {
    const id = req.params.id;
    getById(id, (err, response) => {
      if (err) {
        console.log(err);
        return;
      } else if (response) {
        if (!response) {
          return res.json({
            success: 0,
            message: "Record not Found"
          });
        }
        return res.json(response);
      }
    });
  },

  sendMailById: (req, res) => {
    const pedido_id = req.params.id;
    const body = req.body;
    sendMailById(body.email, pedido_id, (err, response) => {
      return res.json(err);
    });
  },

  reportById: (req, res) => {
    const pedido_id = req.params.id;
    const body = req.body;
    reportById(pedido_id, (err, response) => {
      return res.pipe(response);
    });
  },

  update: (req, res) => {
    const body = req.body;
    update(body, (err, response) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          success: 0,
          message: err.sqlMessage
        });
      } else if (response) {
        return res.json({
          success: 1,
          message: "updated successfully"
        });
      }
    });
  },

  deleteItem: (req, res) => {
    const id = req.params.id;
    deleteItem(id, (err, response) => {
      if (err) {
        console.log(err);
        return res.json({
          success: 0,
          message: err.sqlMessage
        });
      } else if (response) {
        if (response.affectedRows == 0) {
          return res.json({
            success: 0,
            message: "Record Not Found"
          });
        }
        return res.json({
          success: 1,
          message: "deleted successfully"
        });
      }
    });
  }



};
