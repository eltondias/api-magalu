CREATE DATABASE  IF NOT EXISTS `magalu` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `magalu`;
-- MySQL dump 10.13  Distrib 8.0.12, for macos10.13 (x86_64)
--
-- Host: localhost    Database: magalu
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cliente` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) DEFAULT NULL,
  `sexo` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'Elton J D Gonçalves','83205462220','M','elton.jd.goncalves@gmail.com'),(2,'payment Madeira','AGP Savings local','incubate','Isaac_Moreira78@hotmail.com'),(3,'Luvas cross-platform','Specialist verde Queijo','driver','Mariana_Costa@live.com'),(4,'digital e-business','transitional Switchable','Marginal','Esther93@yahoo.com'),(5,'petróleo','Casa Research Architect','set','Eduarda_Saraiva@bol.com.br'),(6,'facilitate Bola','calculating online','Frango','JlioCsar65@live.com'),(7,'calculate','Chile Investor protocol','Plástico cultivate','DaviLucca.Albuquerque@yahoo.com'),(8,'Atum','Auto Director Lead','Avenida payment microchip','Clia_Moreira90@live.com'),(9,'mobile Platinum','Madeira Loan coral','optimize Eletrônicos','Bryan37@bol.com.br'),(10,'Travessa','Cambridgeshire','Account Avon','Paulo_Oliveira73@hotmail.com'),(11,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(12,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(13,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(14,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(15,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(16,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(17,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(18,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(19,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(20,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(21,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(22,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(23,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(24,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(26,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(27,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(28,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(29,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(36,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(37,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(38,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com'),(39,'Elton Gonçalves','83205462220','Msadsadsadsad','elton@gmail.com');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `pedido`
--

DROP TABLE IF EXISTS `pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pedido` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `observacao` varchar(255) DEFAULT NULL,
  `forma_pagamento` varchar(255) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `data` datetime(6),
  `cliente_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pedido__cliente_id` (`cliente_id`),
  CONSTRAINT `fk_pedido__cliente_id` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido`
--

LOCK TABLES `pedido` WRITE;
/*!40000 ALTER TABLE `pedido` DISABLE KEYS */;
INSERT INTO `pedido` VALUES (1,'transmitter sdsds','CHEQUE',62697,'2021-05-21 06:24:26.000000',1),(2,'Sapatos Etiópia copying','CHEQUE',70887,'2021-05-21 07:44:06.000000',2),(3,'XML Assurance National','DINHEIRO',93571,'2021-05-20 22:18:21.000000',3),(4,'marca','DINHEIRO',33326,'2021-05-21 08:11:54.000000',4),(5,'clicks-and-mortar deposit','CHEQUE',97062,'2021-05-20 21:05:27.000000',5),(6,'Toalhas','CARTAO',55097,'2021-05-20 19:39:44.000000',6),(7,'payment holistic','CARTAO',62018,'2021-05-20 19:59:23.000000',7),(8,'Música optimize','DINHEIRO',92343,'2021-05-21 15:17:30.000000',8),(9,'Franc Avenida','CARTAO',25331,'2021-05-21 02:39:34.000000',9),(10,'Toalhas Argentine Ceará','CHEQUE',20196,'2021-05-21 01:41:19.000000',10),(13,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(14,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(15,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(16,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(17,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(18,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(19,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(20,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(21,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(22,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(23,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(24,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(25,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(26,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(27,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(28,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(29,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1),(30,'transmitter','CHEQUE',37000,'2021-05-21 06:24:26.000000',1);
/*!40000 ALTER TABLE `pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `produto` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `cor` varchar(255) DEFAULT NULL,
  `tamanho` varchar(255) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (1,'Iphone X','Preto','grande',9000),(2,'Synergistic','Saint Fresco Mobility','Buckinghamshire Aço',41128),(3,'synthesizing Technician Cambridgeshire','Credit Granito','parse Customer client-driven',49400),(4,'Inteligente next-generation toolset','Aço','açafrão programming',23204),(5,'up Rodovia Incrível','National connecting Technician','one-to-one Won navigating',5081),(6,'Avon help-desk benchmark','platforms online system','açafrão superstructure',91880),(7,'card','Global discrete','Senior online',19176),(8,'à Managed Auto','withdrawal','capability client-server',86355),(9,'JSON Representative Borders','generation','niches',41653),(10,'Sheqel synergize','Macio Auto blockchains','Aço Berkshire',78897),(11,'Iphone X','Preto','grande',75584),(12,'Iphone X','Preto','grande',75584),(13,'Iphone X','Preto','grande',75584);
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto_pedido`
--

DROP TABLE IF EXISTS `produto_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `produto_pedido` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `quantidade` int DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `pedido_id` bigint NOT NULL,
  `produto_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_produto_pedido__pedido_id` (`pedido_id`),
  KEY `fk_produto_pedido__produto_id` (`produto_id`),
  CONSTRAINT `fk_produto_pedido__pedido_id` FOREIGN KEY (`pedido_id`) REFERENCES `pedido` (`id`),
  CONSTRAINT `fk_produto_pedido__produto_id` FOREIGN KEY (`produto_id`) REFERENCES `produto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto_pedido`
--

LOCK TABLES `produto_pedido` WRITE;
/*!40000 ALTER TABLE `produto_pedido` DISABLE KEYS */;
INSERT INTO `produto_pedido` VALUES (1,2,18000,29,1),(2,1,1000,29,2),(3,2,18000,30,1),(4,1,1000,30,2);
/*!40000 ALTER TABLE `produto_pedido` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-23 23:55:27
